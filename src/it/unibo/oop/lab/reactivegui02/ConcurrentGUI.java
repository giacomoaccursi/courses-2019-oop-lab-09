package it.unibo.oop.lab.reactivegui02;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;




public class ConcurrentGUI extends JFrame{
	
	/**
	 * 
	 */
	private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.1;
	private static final long serialVersionUID = 1L;
	private final JLabel label = new JLabel("0"); 
	private final JButton up = new JButton("Up");
	private final JButton down = new JButton("Down");
	private final JButton  stop = new JButton("Stop");
	

	public ConcurrentGUI() {
		super(); 
		final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize(); 
		this.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        JPanel panel = new JPanel(); 
        panel.add(label); 
        panel.add(up); 
        panel.add(down); 
        panel.add(stop); 
        this.getContentPane().add(panel); 
        this.setVisible(true);
        
        final Agent agent = new Agent(); 
        new Thread(agent).start(); 
        
        
        up.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				agent.increment();  				
			}
		});
    
        down.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				agent.decrement(); 				
			}
		});
    
        stop.addActionListener(new ActionListener() {
	
		@Override
		public void actionPerformed(ActionEvent arg0) {
			agent.stopCounting(); 
			up.setEnabled(false);
			down.setEnabled(false);
		}
        });

    }
	private class Agent implements Runnable{

    	
    	private volatile boolean stop; 
    	private volatile int counter;
    	private boolean option; 
    	private boolean start; 
     
    
    	
		
    	public void run() {
			while(!this.stop) {
				try {
					
					SwingUtilities.invokeAndWait(new Runnable() {
						
						@Override
						public void run() {
							ConcurrentGUI.this.label.setText(Integer.toString(Agent.this.counter));
							
						}
					});
					
					if (start) {
						if (this.option) {
							this.counter++; 
						} else {
							this.counter--; 
						}
					}
					
					Thread.sleep(100); 
				} catch (InvocationTargetException | InterruptedException e){
					e.printStackTrace();
				}
			}
			
		}
		public void decrement() {
			this.start = true; 
			this.option = false;  
		}
		
		public void increment() {
			this.start = true; 
			this.option = true;    
		}
		
		public void stopCounting() {
			this.stop = true; 
		}
		
		
	}
}
